﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardController : MonoBehaviour {

	public int columns;
	public int rows;

	public GameObject[] floors;
	public GameObject[] enemies;

	private Transform gameBoard;
	private List<Vector3> obstaclesGrid;

	void Awake () {
		obstaclesGrid = new List<Vector3>();
	}
	
	void Update () {
	
	}

	private void InitializeObstaclePositions()
	{
		obstaclesGrid.Clear ();

		for (int x = 2; x < columns - 2; x++) 
		{
			for(int y = 2; y < rows - 2; y++)
			{
				obstaclesGrid.Add(new Vector3(x, y, 0f));
			}
		}
	}
	private void SetupGameBoard()
	{
		gameBoard = new GameObject("Game Board").transform;

		for (int x = 0; x < columns; x++) 
		{
			for(int y = 0; y < rows; y++)
			{
				GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
				floorTile.transform.SetParent(gameBoard);
			}
		}
	}

	private void SetRandomObstaclesOnGrid(GameObject[] obstaclesArray, int minimum, int maximum)
	{
		int obstaclesCount = Random.Range (minimum, maximum + 1);

		if (obstaclesCount > obstaclesGrid.Count) 
		{
			obstaclesCount = obstaclesGrid.Count;
		}

		for (int index = 0; index < obstaclesCount; index++) 
		{
			GameObject selectedObstacle = obstaclesArray[Random.Range(0, obstaclesArray.Length)];
			Instantiate(selectedObstacle, SelectedGridPosition(), Quaternion.identity);
		}
	}

	private Vector3 SelectedGridPosition()
	{
		int randomIndex = Random.Range (0, obstaclesGrid.Count);
		Vector3 randomPosition = obstaclesGrid[randomIndex];
		obstaclesGrid.RemoveAt(randomIndex);
		return randomPosition;
	}
	public void SetupLevel(int currentLevel)
	{
		InitializeObstaclePositions ();
		SetupGameBoard ();
		int enemyCount = (int)Mathf.Log(currentLevel, 2);
		SetRandomObstaclesOnGrid (enemies, enemyCount, enemyCount);
	}
}

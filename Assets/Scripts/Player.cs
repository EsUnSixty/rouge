﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class player : MovingObject {
	
	public Text healthText;

	private Animator animator;
	private int playerHealth = 20;
	private int attackPower = 1;

	private int secondsUntilNextLevel = 1;
	
	
	protected override void Start()
	{
		base.Start();
		animator = GetComponent<Animator> ();
		playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = "Health: " + playerHealth;
	}
	
	private void onDisable()
	{
		GameController.Instance.playerCurrentHealth = playerHealth;
	}
	
	void Update () {
		if (!GameController.Instance.isPlayerTurn)
		{
			return;
		}
		
		CheckIfGameOver();
		
		int xAxis = 0;
		int yAxis = 0;
		
		xAxis = (int)Input.GetAxisRaw("Horizontal");
		yAxis = (int)Input.GetAxisRaw("Vertical");
		
		if (xAxis != 0)
		{
			yAxis = 0; 
		}
		
		if (xAxis != 0 || yAxis != 0) 
		{
			playerHealth--;
			healthText.text = "Health: " + playerHealth;
			GameController.Instance.isPlayerTurn = false;
		}
	}

	
	private void LoadNewLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	
	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
		animator.SetTrigger("playerHurt");
	}
	
	private void CheckIfGameOver()
	{
		if (playerHealth <= 0) 
		{
			GameController.Instance.GameOver();
			// you dead when you get to zero foo
		}
	}
}











